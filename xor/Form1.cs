﻿using System;
using System.Text;
using System.Windows.Forms;

namespace DES_v1
{
    public partial class Form1 : Form
    {

       
        private const int lengtKey = 8;//длина ключа 
       
        string  value_key,  // значение ключа
                xor_txt,    //зашифрованный текст
                in_txt,     //текст из файла
                padded_txt,
                output_txt;

        public Form1()
        {
            InitializeComponent();
            openFileDialog1.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";//открываем файл
            saveFileDialog1.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";//записываем текст в файл
        }
        //кнопка Сгенерировать ключ
        private void Gen_key_Click(object sender, EventArgs e)
        {  
            string str_for_genarate_key = "abcdefghijklmnopqrstuvwxyzABCDEFJHIJKLMNORSTUYWXYZPQ0123456789!+-£$$%^&*";// строка символов, из которой генерируем ключ
            value_key = GenerateRandKey(str_for_genarate_key, lengtKey);
            key.Text = value_key;
        }
     
        //кнопка "Загрузить файл"
        private void load_file_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = openFileDialog1.FileName;
            // читаем файл в строку
            string fileText = System.IO.File.ReadAllText(filename);
            decr_txt.Text = fileText;
            in_txt = fileText;
            MessageBox.Show("Файл прочитан");
        }

      //кнопка "Выполнить"

        private void start_Click(object sender, EventArgs e)
        {
         
 //зашифрованный текст
            if (radioButton_encr.Checked == true)//шифруем
            {
                padded_txt = padded_Text(in_txt, value_key);    // дополняем текст
                xor_txt = XOR(padded_txt, value_key);           // шифруем текст
                encr_txt.Text = xor_txt;                        // выводим зашифрованный текст в TextBox
            }
            else   // расшифровываем
            {
                output_txt = decrypt(xor_txt, value_key);// расшифровываем текст
                decr_txt.Text=output_txt;       // выводим сообшение
            }
        }
        //выгрузка в файл
        private void unload_file_BUTTON(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = saveFileDialog1.FileName;

            // сохраняем текст в файл
            System.IO.File.WriteAllText(filename, encr_txt.Text);

            MessageBox.Show("Файл сохранен");

        }

        //функция генерации ключа
        private string GenerateRandKey(string data, int len)//генерация ключа из строки
        {
            Random rnd = new Random();
            StringBuilder sb = new StringBuilder(len);

            int Position = 0;

            for (int i = 0; i < len; i++)
            {
                Position = rnd.Next(0, data.Length - 1);
                sb.Append(data[Position]);
            }
            return sb.ToString();
        }


        //метод дополнения текста
        private string padded_Text(string txt_in, string key)
        {
            int dif = ((key.Length - txt_in.Length % key.Length) % key.Length); //смотрим на сколько нужно дополнить
            string new_data = txt_in;
            for (int i = 0; i < dif; i++)
            {
                new_data += "\0";   //дополняем 

            }

            return new_data;
        }

        //шифрование
        private string XOR(string in_txt, string key)
        {

            var input_date = Encoding.Unicode.GetBytes(in_txt);
            var key_date = Encoding.Unicode.GetBytes(key);
            var res = new byte[input_date.Length];

            for (int i = 0; i < input_date.Length; i++)
                res[i] = (byte)(input_date[i] ^ key_date[i % key_date.Length]);
            
            return Encoding.Unicode.GetString(res);
        }

         //расшифровка
        private string decrypt(string output, string key)
        {
             return XOR(output, key);
            
        }
    }
}






