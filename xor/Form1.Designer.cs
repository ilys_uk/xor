﻿namespace DES_v1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.start = new System.Windows.Forms.Button();
            this.Generate_key = new System.Windows.Forms.Button();
            this.decr_txt = new System.Windows.Forms.TextBox();
            this.encr_txt = new System.Windows.Forms.TextBox();
            this.key = new System.Windows.Forms.TextBox();
            this.radioButton_encr = new System.Windows.Forms.RadioButton();
            this.radioButton_decr = new System.Windows.Forms.RadioButton();
            this.unload_file = new System.Windows.Forms.Button();
            this.load_file = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // start
            // 
            this.start.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Bold);
            this.start.Location = new System.Drawing.Point(540, 267);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(123, 51);
            this.start.TabIndex = 0;
            this.start.Text = "Выполнить";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // Generate_key
            // 
            this.Generate_key.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Generate_key.Location = new System.Drawing.Point(51, 261);
            this.Generate_key.Name = "Generate_key";
            this.Generate_key.Size = new System.Drawing.Size(186, 40);
            this.Generate_key.TabIndex = 1;
            this.Generate_key.Text = "Сгенерировать ключ";
            this.Generate_key.UseVisualStyleBackColor = true;
            this.Generate_key.Click += new System.EventHandler(this.Gen_key_Click);
            // 
            // decr_txt
            // 
            this.decr_txt.Location = new System.Drawing.Point(12, 12);
            this.decr_txt.Multiline = true;
            this.decr_txt.Name = "decr_txt";
            this.decr_txt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.decr_txt.Size = new System.Drawing.Size(832, 238);
            this.decr_txt.TabIndex = 2;
            // 
            // encr_txt
            // 
            this.encr_txt.Location = new System.Drawing.Point(12, 338);
            this.encr_txt.Multiline = true;
            this.encr_txt.Name = "encr_txt";
            this.encr_txt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.encr_txt.Size = new System.Drawing.Size(832, 238);
            this.encr_txt.TabIndex = 3;
            // 
            // key
            // 
            this.key.Location = new System.Drawing.Point(67, 307);
            this.key.Name = "key";
            this.key.Size = new System.Drawing.Size(152, 22);
            this.key.TabIndex = 4;
            // 
            // radioButton_encr
            // 
            this.radioButton_encr.AutoSize = true;
            this.radioButton_encr.Checked = true;
            this.radioButton_encr.Font = new System.Drawing.Font("Arial", 7.8F);
            this.radioButton_encr.ForeColor = System.Drawing.SystemColors.WindowText;
            this.radioButton_encr.Location = new System.Drawing.Point(680, 271);
            this.radioButton_encr.Name = "radioButton_encr";
            this.radioButton_encr.Size = new System.Drawing.Size(116, 20);
            this.radioButton_encr.TabIndex = 1;
            this.radioButton_encr.TabStop = true;
            this.radioButton_encr.Text = "Шифрование";
            this.radioButton_encr.UseVisualStyleBackColor = true;
            // 
            // radioButton_decr
            // 
            this.radioButton_decr.AutoSize = true;
            this.radioButton_decr.Font = new System.Drawing.Font("Arial", 7.8F);
            this.radioButton_decr.Location = new System.Drawing.Point(680, 297);
            this.radioButton_decr.Name = "radioButton_decr";
            this.radioButton_decr.Size = new System.Drawing.Size(131, 20);
            this.radioButton_decr.TabIndex = 0;
            this.radioButton_decr.Text = "Дешифрование";
            this.radioButton_decr.UseVisualStyleBackColor = true;
            // 
            // unload_file
            // 
            this.unload_file.Font = new System.Drawing.Font("Arial", 7.8F);
            this.unload_file.Location = new System.Drawing.Point(389, 270);
            this.unload_file.Name = "unload_file";
            this.unload_file.Size = new System.Drawing.Size(107, 48);
            this.unload_file.TabIndex = 6;
            this.unload_file.Text = "Выгрузить в файл";
            this.unload_file.UseVisualStyleBackColor = true;
            this.unload_file.Click += new System.EventHandler(this.unload_file_BUTTON);
            // 
            // load_file
            // 
            this.load_file.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.load_file.Location = new System.Drawing.Point(276, 270);
            this.load_file.Name = "load_file";
            this.load_file.Size = new System.Drawing.Size(107, 48);
            this.load_file.TabIndex = 7;
            this.load_file.Text = "Загрузить файл";
            this.load_file.UseVisualStyleBackColor = true;
            this.load_file.Click += new System.EventHandler(this.load_file_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 591);
            this.Controls.Add(this.radioButton_encr);
            this.Controls.Add(this.load_file);
            this.Controls.Add(this.radioButton_decr);
            this.Controls.Add(this.unload_file);
            this.Controls.Add(this.key);
            this.Controls.Add(this.encr_txt);
            this.Controls.Add(this.decr_txt);
            this.Controls.Add(this.Generate_key);
            this.Controls.Add(this.start);
            this.Name = "Form1";
            this.Text = "Шифрование xor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Button Generate_key;
        private System.Windows.Forms.TextBox decr_txt;
        private System.Windows.Forms.TextBox encr_txt;
        private System.Windows.Forms.TextBox key;
        private System.Windows.Forms.RadioButton radioButton_encr;
        private System.Windows.Forms.RadioButton radioButton_decr;
        private System.Windows.Forms.Button unload_file;
        private System.Windows.Forms.Button load_file;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

